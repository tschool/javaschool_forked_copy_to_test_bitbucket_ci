package com.tsystems.javaschool.tasks.calculator;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// class for validating whole statement and plain statements
public class Validator {

    public static boolean validateWholeStatement(String statement) {
        return (checkEmptyString(statement) &&
                checkIllegalSymbols(statement) &&
                checkParenthesis(statement) &&
                checkDotSymbol(statement));
    }

    public static boolean checkEmptyString(String statement) {
        if ("".equals(statement) || statement == null) {
            try {
                throw new IllegalArgumentException("Empty line");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
    public static boolean checkParenthesis(String statement) {
        char[] symbols = statement.toCharArray();
        char open = '(';
        char close = ')';
        int countOpen = 0;
        int countClose = 0;
        for (char symbol : symbols) {
            if (symbol == open) {
                countOpen++;
            }
            if (symbol == close) {
                countClose++;
            }
        }
        if (countOpen != countClose) {
            try {
                throw new IllegalArgumentException("Something wrong with parenthesis");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static boolean checkIllegalSymbols(String statement) {
        if (!statement.matches("[0-9()+\\-*/. ]+$")) {
            try {
                throw new IllegalArgumentException("Illegal characters");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static boolean checkDotSymbol(String statement) {
        // test if not number goes after dot
        String regExPattern = ".*\\d\\.\\D.*";
        Pattern p = Pattern.compile(regExPattern);
        Matcher m = p.matcher(statement);
        if (m.find()) {
            try {
                throw new IllegalArgumentException("Illegal symbol goes after dot");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static boolean validatePlainStatement(String plainStatement) {
        try {
            if (plainStatement == null) {
                try {
                    throw new IllegalArgumentException("Wrong parenthesis order )(");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            //test if ends not with number
            if (!plainStatement.matches(("^.*\\d$"))) {
                throw new ParseException(plainStatement, plainStatement.length() - 1);
            }
            // test if starts not from number or minus
            if (!plainStatement.matches("^-*\\d.+?")) {
                throw new ParseException(plainStatement, 0);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println(String.format("Expression is wrong on index %s", e.getErrorOffset()));
            return false;
        }
        return true;
    }


}