package com.tsystems.javaschool.tasks.calculator.command;

import java.util.List;

public class MinusCommand extends AbstractBasicArithmeticCommand {
    @Override
    // recursively do all subtracting operations in plain statement and each time insert result of evaluation in it
    public String doOperation(String plainStatement) {
        String pattern = "(-?\\d+\\.?\\d*)-(-?\\d+\\.?\\d*)"; // pattern to get two operands

        List<Float> listOfOperands = this.getTwoOperands(plainStatement, pattern);
        if (listOfOperands.isEmpty()) {
            return plainStatement;
        }
        float left = listOfOperands.get(0);
        float right = listOfOperands.get(1);
        float result = left - right;
        return doOperation(plainStatement.replaceFirst(pattern, String.valueOf(result)));
        }

    }

